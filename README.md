# Dubbio

Dubbio is a wrapper to various command line tools for [D language](https://dlang.org/).

## General usage

```
dubbio [--version] [<command>] [<options...>] [-- [<application arguments...>]]
```

## Commands

|          |                    | Equivalent to            |
| ---      | ---                | ---                      |
|          |                    | dub build                |
| -b       | --build            | dub build                |
| -x       | --run              | dub run                  |
| -t       | --test             | dub test                 |
| -C       | --clean            | dub clean                |
| -T       | --tags             | dscanner --ctags src     |
| -T `dir` | --tags `dir`       | dscanner --ctags `dir`   |
| -S       | --styleCheck       | dscanner -S src          |
| -S `dir` | --styleCheck `dir` | dscanner -S `dir`        |
|          | --ver              | *Print package version*  |
| -z       | --upx              | upx --no-progress `name` |

## Options
|               |                  | Equivalent to                            |
| ---           | ---              | ---                                      |
| -c=`value`    | --config=`value` | dub -c`value`                            |
| -u=`lib:suba` | --sub=`lib:suba` | dub `lib:suba`                           |
| -d            | --onlysrc        | limit command into `src` or `source` dir |
| -e            | --nodeps         | dub --nodeps                             |
| -f            | --force          | dub --force                              |
| -l            | --ldc            | dub --compiler=ldc                       |
|               | --ldc2           | dub --compiler=ldc2                      |
| -a            | --arm            | dub --compiler=ldc --config=arm          |
| -r            | --release        | dub -brelease                            |
| -s            | --single         | dub --build-mode=single-file --parallel  |
| -v0           | --verbosity=0    | dub -q                                   |
| -v1           | --verbosity=1    | dub                                      |
| -v2           | --verbosity=2    | dub --verbose                            |
| -v3           | --verbosity=3    | dub --vverbose                           |


## Alphabetical list
|               |                    | Equivalent to                           |
| ---           | ---                | ---                                     |
|               | --ldc2             | dub --compiler=ldc2                     |
|               | --ver              | *Print package version*                 |
| -C            | --clean            | dub clean                               |
| -S            | --styleCheck       | dscanner -S src                         |
| -S `dir`      | --styleCheck `dir` | dscanner -S `dir`                       |
| -T            | --tags             | dscanner --ctags src                    |
| -T `dir`      | --tags `dir`       | dscanner --ctags `dir`                  |
| -a            | --arm              | dub --compiler=ldc --config=arm         |
| -b            | --build            | dub build                               |
| -c=`value`    | --config=`value`   | dub -c`value`                           |
| -d            | --ddox             | dub build -q -bddox                     |
| -e            | --nodeps           | dub --nodeps                            |
| -f            | --force            | dub --force                             |
| -l            | --ldc              | dub --compiler=ldc                      |
| -m            | --onlysrc          |                                         |
| -r            | --release          | dub -brelease                           |
| -s            | --single           | dub --build-mode=single-file --parallel |
| -t            | --test             | dub test                                |
| -u=`lib:suba` | --sub=`lib:suba`   | dub `lib:suba`                          |
| -v0           | --verbosity=0      | dub -q                                  |
| -v1           | --verbosity=1      | dub                                     |
| -v2           | --verbosity=2      | dub --verbose                           |
| -v3           | --verbosity=3      | dub --vverbose                          |
| -x            | --run              | dub run                                 |
| -z            | --upx              | upx --no-progress `name`                |

## Examples
| Command                | Description                                           |
| ---                    | ---                                                   |
| dubbio -r              | Compile in release mode                               |
| dubbio -l              | Compile in debug mode with ldc                        |
| dubbio -rl             | Compile in release mode with ldc                      |
| dubbio -c=demo         | Compile with `demo` configuration                     |
| dubbio -r -u="lib:sub" | Compile `sub` subpackage in release mode              |
| dubbio -ra             | Compile in release mode with ldc and with `arm` conf. |
| dubbio -T              | Create  `tags` file                                   |
| dubbio -T -m           | Create  `tags` file only for `src` or `source` dir    |


## Installation
First install:
- [dub](https://github.com/dlang/dub)
- [dscanner](https://github.com/dlang-community/D-Scanner)
- [upx](https://upx.github.io/) in order to use `-z` option

then clone:
```
$ git clone git@github.com:o3o/dubbio.git
```

finally compile and install:
```
$ cd dubbio
$ ./install.sh
````

## Configure vim
In order to set `dubbio` to run on the current file when `:make` is invoked, [add](https://codeyarns.com/2015/05/04/how-to-set-makeprg-based-on-filetype-in-vim/):
```
autocmd Filetype d setlocal makeprg=dubbio
```
in your `.vimrc` file.
