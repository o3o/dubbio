module dubbio.semver;

enum VERSION = "0.9.5";
enum TAG_VERSION = "v" ~ VERSION;
