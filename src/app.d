import std.getopt;
import std.process;
import std.stdio;
import std.string : format;

import dubbio.semver;

struct Args {
   bool v;
   bool ver;
   bool pass;
   bool build;
   bool run;
   bool test;
   bool tags;
   bool styleCheck;
   bool clean;
   bool ldc;
   bool ldc2;
   bool arm;
   bool rel;
   bool ddox;
   string config;
   string sub;
   bool force;
   bool singleFile;
   bool noDeps;
   bool dry;
   bool dry2;
   bool upx;
   bool list;
   int verbosityLevel;
   bool onlySrc;
   string[] other;
}

void main(string[] args) {
   auto opt = Args();
   GetoptResult result = getopt(args,
         std.getopt.config.bundling,
         std.getopt.config.passThrough,
         std.getopt.config.caseSensitive,
         "version", "Print version and exit", &opt.v,
         "ver", "Print sw version and exit", &opt.ver,
         "pass|p", "Pass through to dub", &opt.pass,
         "build|b", "Build", &opt.build,
         "run|x", "Run", &opt.run,
         "test|t", "test", &opt.test,
         "tags|T", "Make tags", &opt.tags,
         "styleCheck|S" ,"dscanner -S", &opt.styleCheck,
         "clean|C", "Clean", &opt.clean,
         "ldc|l", "Uses 'ldc' compiler", &opt.ldc,
         "ldc2", "Uses 'ldc2' compiler", &opt.ldc2,
         "arm|a", "Equivalent to '-l -c=arm'", &opt.arm,
         "release|r", "Uses 'release' build", &opt.rel,
         "ddox|d", "Uses 'ddox' build", &opt.ddox,
         "config|c", "Builds the specified configuration", &opt.config,
         "sub|u", "Builds subpackages", &opt.sub,
         "force|f", "Forces a recompilation", &opt.force,
         "single|s", "Add --build-mode=single-file --parallel", &opt.singleFile,
         "nodeps|e", "Add --nodeps", &opt.noDeps,
         "upx|z", "Compress with UPX", &opt.upx,
         "list", "List d files", &opt.list,
         "verbose|v", "Verbosity level", &opt.verbosityLevel,
         "just-print|n", "Just print", &opt.dry,
         "just-print-v", "Just print verbose", &opt.dry2,
         "onlysrc|m", "Limit cmd to src or source dir", &opt.onlySrc,
         );

   opt.other = args[1 .. $]; // toglie dubbio

   if (result.helpWanted) {
      defaultGetoptPrinter("Dub wrapper", result.options);
   } else {
      string[] a;
      if (opt.tags) {
         createTags(opt);
         return;
      } else if (opt.v) {
         writeln("Dubbio version %s".format(VERSION));
         return;
      } else if (opt.ver) {
         writeln(getVersion());
         return;
      } else if (opt.styleCheck) {
         a = getStyle(opt);
      } else if (opt.upx) {
         a = getUpx();
      } else if (opt.list) {
         import std.file;
         import std.string : endsWith;
         import std.algorithm.iteration : filter;
         auto dFiles = dirEntries("", SpanMode.depth).filter!(f => f.name.endsWith(".d"));
         foreach (d; dFiles) {
            writeln(d.name);
         }
         return;
      } else {
         a ~= "dub";
         if (opt.pass) {
            a = a.getAppArgs(args, "");
         } else {
            a = a.getCommand(opt).getOptions(opt).getAppArgs(args);
         }
      }

      if (opt.dry) {
         a.print();
      } else if (opt.dry2) {
         a.print();
         writeln();
         foreach (i, c; a) {
            writeln(i, ":", c);
         }
      } else {
         auto pid = spawnProcess(a);
         scope(exit) wait(pid);
      }
   }
}

private string[] getCommand(string[] cmd, Args opt) {
   if (opt.run) {
      cmd ~= "run";
   } else if (opt.test) {
      cmd ~= "test";
   } else if (opt.clean) {
      cmd ~= "clean";
   } else if (opt.pass) {
      cmd ~= "";
   } else {
      cmd ~= "build";
   }
   return cmd;
}
@safe unittest {
   string[] a = ["dub"];
   Args opt = {run: true};
   a = a.getCommand(opt);
   assert(a.length == 2);
   assert(a[0] == "dub");
   assert(a[1] == "run");
}

private string[] getOptions(string[] cmd, Args opt) {
   switch (opt.verbosityLevel) {
      case 0:
         cmd ~= "-q";
         break;
      case 2:
         cmd ~= "--verbose";
         break;
      case 3:
         cmd ~= "--vverbose";
         break;
      default:
   }
   if (opt.ldc) {
      cmd ~= "--compiler=ldc";
   } else if (opt.ldc2) {
      cmd ~= "--compiler=ldc2";
   } else if (opt.arm) {
      cmd ~= "--config=arm";
      cmd ~= "--compiler=ldc";
   }
   if (opt.force) {
      cmd ~= "-f";
   }
   if (opt.noDeps) {
      cmd ~= "--nodeps";
   }
   if (opt.singleFile) {
      cmd ~= "--build-mode=singleFile";
      cmd ~= "--parallel";
   }
   if (opt.rel) {
      cmd ~= "-brelease";
   }
   if (opt.ddox) {
      cmd ~= "-bddox";
   }

   if (opt.config.length) {
      cmd ~= "-c" ~ opt.config;
   }
   if (opt.sub.length) {
      cmd ~= opt.sub;
   }

   return cmd;
}
@safe unittest {
   string[] a = ["dub"];
   Args opt = {ldc: true, ldc2: true, rel: true};
   a = a.getOptions(opt);
   assert(a.length == 3);
   assert(a[0] == "dub");
   assert(a[1] == "--compiler=ldc");
   assert(a[2] == "-brelease");
}
@safe unittest {
   string[] a = ["dub"];
   Args opt = {ldc: false, ldc2: true, rel: true};
   a = a.getOptions(opt);
   assert(a.length == 3);
   assert(a[0] == "dub");
   assert(a[1] == "--compiler=ldc3");
   assert(a[2] == "-brelease");
}
@safe unittest {
   string[] a = ["dub"];
   Args opt = {ldc: false, ldc2: true, rel: true, parallel: true};
   a = a.getOptions(opt);
   assert(a.length == 4);
   assert(a[0] == "dub");
   assert(a[1] == "--compiler=ldc3");
   assert(a[2] == "--build-mode=single-file --parallel");
   assert(a[3] == "-brelease");
}

private string[] getAppArgs(string[] cmd, string[] args, string sep = "--") {
   if (args.length > 1) {
      if (sep.length) {
         cmd ~= sep;
      }
      foreach (i; 1 .. args.length) {
         cmd ~= "%-s".format(args[i]);
      }
   }
   return cmd;
}
@safe unittest {
   string[] a = ["dub"];
   string[] aa = ["dubbio", "cul", "piss"];
   a = a.getAppArgs(aa);

   assert(a.length == 4);
   assert(a[0] == "dub");
   assert(a[1] == "--");
   assert(a[2] == "cul");
   assert(a[3] == "piss");
}

private void createTags(Args args) {
   import std.file;
   import std.string : endsWith;
   import std.algorithm.iteration : filter;

   auto a = ["dscanner", "--ctags"];
   string src = getDefaultSrc();
   if (src.length && args.onlySrc) {
      a ~= src;
   } else {
      auto dFiles = dirEntries("", SpanMode.depth).filter!(f => f.name.endsWith(".d"));
      foreach (d; dFiles) {
         a ~= d;
      }
   }

   if (args.dry) {
      a.print();
   } else {
      auto dscan = execute(a);
      if (dscan.status != 0) {
         writeln("Compilation failed:\n", dscan.output);
      } else {
         auto file = File("tags", "w");
         file.write(dscan.output);
      }
   }
}

/**
 * Returns `source` or `src` or empty if these does't exist
 */
private string getDefaultSrc() {
   import std.file : exists;
   if (exists("src")) {
      return "src";
   } else if (exists("source")) {
      return "source";
   } else {
      return "";
   }
}

private string[] getStyle(Args args) {
   string[] a = ["dscanner", "-S"];
   if (args.other.length) {
      a ~= args.other[0];
   } else {
      a ~= getDefaultSrc();
   }
   return a;
}

void print(string[] a) {
   writefln("!! %-(%s %)".format(a));
}

string getVersion() {
   import std.file;
   import std.path : baseName;
   import std.regex;
   try {
      auto dFiles = dirEntries("src", "*.d", SpanMode.depth);
      foreach (s; dFiles) {
         if (s.baseName == "semver.d") {
            auto semver = readText(s);
            auto exp = regex(`\d+\.\d+\.\d+(-\w+\.\d)?`); // multi regex
            auto m = semver.matchAll(exp);
            return m.front[0];
         }
      }
   } catch (FileException ex) {
      writeln(ex);
   }
   return "???";
}

private string[] getUpx() {
   return ["upx", "--no-progress", getAppName()];
}

private string getAppName() {
   import std.file : readText;
   import std.regex;

   auto f = readText("dub.sdl");
   auto exp = regex(`^name\s+"(?P<name>\w+)"`);
   auto c = matchFirst(f, exp);

   return c["name"];
}
