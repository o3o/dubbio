import unit_threaded;
import std.stdio;

@("not-eq")
unittest {
   1.shouldNotEqual(5);
   5.shouldNotEqual(1);
   3.shouldEqual(3);
   2.shouldEqual(2);
}

@("eq")
unittest {
   1.shouldEqual(1);
   shouldEqual(1.0, 1.0);
   "foo".shouldEqual("foo");
}
